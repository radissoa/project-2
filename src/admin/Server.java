package admin;

import java.net.InetAddress;

public class Server {
	
	String name;
	InetAddress IPAddress;
	int port;
	int coordPort;
	
	public Server(String name, InetAddress IPAddress, int port, int coordPort) {
		this.name = name;
		this.IPAddress = IPAddress;
		this.port = port;
		this.coordPort = coordPort;
	}

	public String getName() {
		return name;
	}

	public InetAddress getIPAddress() {
		return IPAddress;
	}

	public int getPort() {
		return port;
	}

	public int getCoordPort() {
		return coordPort;
	}
	
}
