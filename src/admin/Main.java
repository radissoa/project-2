package admin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Main{

	public static HashMap<String,Server> servers = new HashMap<String,Server>();

	public static Scanner scanner = new Scanner(System.in);

	static boolean debug = false;
	
	static boolean creating = false;
	
	public static void main(String[] args) {

		
		if(args.length != 1){
			if(args[1].equals("-d"))
				debug = true;
		}
		
		ServerSocket listeningSocket = null;

		Socket newConn = null;

		BufferedReader reader = null;
		BufferedWriter writer = null;
		
		JSONParser parser = new JSONParser();
		
		try {

			listeningSocket = new ServerSocket(Integer.parseInt(args[0]));
			
			while(true){
				if(debug){
					System.out.println("Waiting for a new connection...");
				}
				newConn = listeningSocket.accept();

				reader = new BufferedReader(new InputStreamReader(newConn.getInputStream(), "UTF-8"));

				if(debug){
					System.out.println("Waiting and reading the request...");
				}
				JSONObject request = (JSONObject)parser.parse(reader.readLine());
				if(debug)
					System.out.println(request.toJSONString());
				if(debug){
					System.out.println("Computing answer...");
				}
				JSONObject answer = null;
				if(request.get("type").equals("newserver"))
					if(!creating)
						answer = createServer(request);
				else
					throw new IllegalArgumentException("Illegal argument...");
				if(debug){
					System.out.println("Sending the answer...");
				}
				if(debug)
					System.out.println(answer.toJSONString());
				writer = new BufferedWriter(new OutputStreamWriter(newConn.getOutputStream(), "UTF-8"));
				
				writer.write(answer.toJSONString() + "\n");
				writer.flush();
				
				//TODO wait for the server to say it is finished
				
				creating = false;
			}

		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}finally{
			try {
				if(!newConn.isClosed())
					newConn.close();
				if(listeningSocket != null)
					listeningSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	@SuppressWarnings("unchecked")
	private synchronized static JSONObject createServer(JSONObject request) {
		String name = (String) request.get("serverid");
		if(servers.containsKey(name)){
			if(debug)
				System.out.println("Refusing the creation of " + name + "...");
			return refuse();
		}
		if(debug)
			System.out.println("Accepting the creation of " + name + "...");
		InetAddress IPAddress = null;
		try {
			IPAddress = InetAddress.getByName((String)request.get("ip"));
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		int port = Integer.parseInt((String)request.get("port"));
		int coordPort = Integer.parseInt((String)request.get("coordport"));
		Server s = new Server(name, IPAddress, port, coordPort);
		JSONObject grant = grant();
		JSONArray array = new JSONArray();
		Iterator<Server> it = servers.values().iterator();
		Server s1;
		while(it.hasNext()){
			s1 = it.next();
			array.add(buildServerJSON(s1));
		}
		grant.put("servers", array);
		servers.put(name, s);
		creating = true;
		return grant;
	}

	@SuppressWarnings("unchecked")
	private static JSONObject buildServerJSON(Server s) {
		JSONObject o = new JSONObject();
		o.put("serverid", s.name);
		o.put("ip", s.IPAddress.getHostName());
		o.put("port", "" + s.port);
		o.put("coordport", "" + s.coordPort);
		return o;
	}

	@SuppressWarnings("unchecked")
	private static JSONObject grant() {
		JSONObject grant = new JSONObject();
		grant.put("type", "newserver");
		grant.put("approved", "true");
		return grant;
	}

	@SuppressWarnings("unchecked")
	private static JSONObject refuse() {
		JSONObject refuse = new JSONObject();
		refuse.put("type", "newserver");
		refuse.put("approved", "false");
		return refuse;
	}
}