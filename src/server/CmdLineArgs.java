package server;

import org.kohsuke.args4j.Option;

/**
 * Arguments
 * @author radissoa
 *
 */
public class CmdLineArgs {

	@Option(required=true, name="-h", usage="Admin's host")
	private String host;	
	
	@Option(required=true, name="-p", usage="Admin's port")
	private String port;
	
	@Option(required=true, name="-n", usage="Server's name")
	private String name;
	
	@Option(required=true, name="-l", usage="Configuration File")
	private String confPath;

	public String getHost() {
		return host;
	}

	public String getPort() {
		return port;
	}

	public String getName() {
		return name;
	}

	public String getConfPath() {
		return confPath;
	}
	
}
