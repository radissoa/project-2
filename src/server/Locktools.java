package server;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.ArrayList;

import org.json.simple.JSONObject;

public abstract class Locktools {

	boolean checkName(String name){

		if(!name.matches("[A-Za-z]\\w{2,15}")){
			return false;
		}
		
		//check if the asked name is used on the server
		if(existingOnServer(name)){
			return false;
		}

		//Ask the others servers if the name is available
		ArrayList<Socket> sockets = new ArrayList<Socket>();
		try {
			//Launching sockets to the other servers
			for(DefSocket dS : ServerIdentity.otherServers.values()){
				sockets.add(new Socket(dS.address, dS.port));
			}
			//the question
			BufferedWriter writer = null;
			JSONObject lock = generateJSONLock(name);
			for(Socket s : sockets){
				writer = new BufferedWriter(new OutputStreamWriter(s.getOutputStream(), "UTF-8"));
				write(writer,lock.toJSONString());
			}
			boolean result = waitForAnswers(name);
			for(Socket s : sockets){
				if(s !=null){
					try{
						s.close();
					}catch(IOException e){
						e.printStackTrace();
					}
				}
			}
			broadcastServer(releaselockJSON(result, name));
			return result;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	static void broadcastServer(JSONObject object){
		ArrayList<Socket> sockets = new ArrayList<Socket>();
			//Launching sockets to the other servers
			for(DefSocket dS : ServerIdentity.otherServers.values()){
				try {
					sockets.add(new Socket(dS.address, dS.port));					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			//signal the release
			BufferedWriter writer = null;
			for(Socket s : sockets){
				try {
					writer = new BufferedWriter(new OutputStreamWriter(s.getOutputStream(), "UTF-8"));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				write(writer,object.toJSONString());
				try {
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			for(Socket s : sockets){
				if(s !=null){
					try{
						s.close();
					}catch(IOException e){
						e.printStackTrace();
					}
				}
			}
	}

	abstract protected JSONObject releaselockJSON(boolean approved, String name);

	abstract boolean existingOnServer(String name);

	abstract protected JSONObject generateJSONLock(String name);

	abstract protected boolean waitForAnswers(String name) throws InterruptedException;

	abstract protected JSONObject generateJSONVote(String name);
		
	static protected void write(BufferedWriter writer, String msg) {
		try {
			writer.write(msg + "\n");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}