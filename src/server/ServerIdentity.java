package server;

import java.io.*;
import java.net.InetAddress;
import java.util.HashMap;

/**
 * Stores all server properties
 * @author radissoa
 *
 */
public class ServerIdentity {

	static String name;
	static InetAddress myIp;
	static int clientPort;
	static int coordinationPort;
	static HashMap<String,DefSocket> otherServers = new HashMap<String,DefSocket>();
	static HashMap<String,DefSocket> otherServersForClients = new HashMap<String,DefSocket>();

	public ServerIdentity(String confPath) throws FileNotFoundException, IllegalArgumentException, IOException{


		File confFile = new File(confPath);
		FileReader reader = new FileReader(confFile);
		BufferedReader bufRead = new BufferedReader(reader);
		String myLine = null;
		myLine = bufRead.readLine();
		String[] array = myLine.split("\t");
		this.setName(array[0]);
		this.setMyIp(InetAddress.getByName(array[1]));
		this.setClientPort(Integer.parseInt(array[2]));
		this.setCoordinationPort(Integer.parseInt(array[3]));
		bufRead.close();
		reader.close();
	}

	private void setName(String name) {
		ServerIdentity.name = name;
	}

	private void setMyIp(InetAddress myIp) {
		ServerIdentity.myIp = myIp;
	}

	private void setClientPort(int clientPort) {
		ServerIdentity.clientPort = clientPort;
	}

	private void setCoordinationPort(int coordinationPort) {
		ServerIdentity.coordinationPort = coordinationPort;
	}

	static public synchronized void addServer(String name, InetAddress IP, int port, int coordPort){
		addOtherServer(name, new DefSocket(IP, coordPort));
		addOtherServerForClients(name, new DefSocket(IP, port));
	}
	
	static private void addOtherServer(String name, DefSocket s){
		otherServers.put(name, s);
	}
	
	static private void addOtherServerForClients(String name, DefSocket s){
		otherServersForClients.put(name, s);
	}
}
