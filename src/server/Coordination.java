package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class Coordination extends Thread {

	@SuppressWarnings("unchecked")
	public JSONObject process(JSONObject obj){
		JSONObject newObj = null;
		switch((String)obj.get("type")){
		case "newserver":
			try {
				ServerIdentity.addServer((String)obj.get("serverid"), 
						InetAddress.getByName((String)obj.get("ip")), 
						Integer.parseInt((String)obj.get("port")), 
						Integer.parseInt((String)obj.get("coordport")));
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (UnknownHostException e) {
				e.printStackTrace();
			}
			newObj = new JSONObject();
			newObj.put("type", "newserverok");
			newObj.put("serverid", ServerIdentity.name);
			JSONArray chatRooms = new JSONArray();
			for(ChatRoom cRoom : ChatRoomsHandler.getInstance().getChatRoomsOnServer()){
				chatRooms.add(cRoom.name);
			}
			newObj.put("chatrooms", chatRooms);
			break;
		case "newserverok":
			try {
				Server.servMsgs.put(new ServMsg((String)obj.get("serverid"), (JSONArray)obj.get("chatrooms")));
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			break;
		case "lockidentity":
			if(!obj.containsKey("locked")){
				//process the vote
				newObj = IdentitiesHandler.getInstance().generateJSONVote((String)obj.get("identity"));
				String[] array = {(String)obj.get("serverid"),(String)obj.get("identity")};
				IdentitiesHandler.getInstance().lockIdentity(array);
			}else{
				//send the response to the adequate queue
				IdentitiesHandler.getInstance().addConnectionMessage((String)obj.get("identity"), new ConnectionMessage(obj));
			}
			break;
		case "releaseidentity":
			String[] array = {(String)obj.get("serverid"),(String)obj.get("identity")};
			IdentitiesHandler.getInstance().unlockIdentity(array);
			break;
		case "lockroomid":
			if(!obj.containsKey("locked")){
				newObj = ChatRoomsHandler.getInstance().generateJSONVote((String)obj.get("roomid"));
				String[] a = {(String)obj.get("serverid"),(String)obj.get("roomid")};
				ChatRoomsHandler.getInstance().lockRoom(a);
			}else{
				ChatRoomsHandler.getInstance().addNewRoomMessage((String)obj.get("roomid"), new RoomMessage(obj));
			}
			break;
		case "releaseroomid":
			String[] array1 = {(String)obj.get("serverid"),(String)obj.get("roomid")};
			if(!((String)obj.get("roomid")).contains("MainHall"))
				ChatRoomsHandler.getInstance().unlockRoom(array1);
			if(((String)obj.get("approved")).equals("true")){
				ChatRoomsHandler.getInstance().acknowledgeRoomCreation(array1);
			}
			break;
		case "deleteroom":
			ChatRoomsHandler.getInstance().acknowledgeRoomDeletion(obj);
			break;
		default:
			throw new IllegalArgumentException("Unsupported coordination message");
		}
		return newObj;
	}

	private void sendAnswer(JSONObject request, JSONObject answer){
		DefSocket s = ServerIdentity.otherServers.get((String)request.get("serverid"));
		Socket server = null;
		try {
			server = new Socket(s.address,s.port);
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(server.getOutputStream(),"UTF-8"));
			writer.write(answer.toJSONString());
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			if(server != null){
				try {
					server.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void run() {

		ServerSocket  coordinationSocket = null;
		Socket coordSocket = null;
		BufferedReader readerCoord = null;
		try {
			coordinationSocket = new ServerSocket(ServerIdentity.coordinationPort);
			while(true){
				coordSocket = coordinationSocket.accept();
				readerCoord = new BufferedReader(new InputStreamReader(coordSocket.getInputStream(), "UTF-8"));
				String msg = readerCoord.readLine();
				JSONParser parser = new JSONParser();
				JSONObject obj = (JSONObject) parser.parse(msg);
				JSONObject answer = process(obj);
				if(answer != null){
					sendAnswer(obj, answer);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(coordinationSocket != null) {
				try {
					coordinationSocket.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

}
