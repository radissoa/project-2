package server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class ChatRoomsHandler extends Locktools{

	private static ChatRoomsHandler instance;

	private LinkedList<ChatRoom> chatRoomsOnServer;
	private LinkedList<String[]> otherChatRooms;
	private MainHall mainHall;
	private ArrayList<String[]> lockedRooms;
	HashMap<String,BlockingQueue<RoomMessage>> roomMsgs;


	private ChatRoomsHandler() {
		this.chatRoomsOnServer = new LinkedList<ChatRoom>();
		this.otherChatRooms = new LinkedList<String[]>();
		this.mainHall = new MainHall(ServerIdentity.name);
		//make public the server's main hall
		broadcastServer(releaselockJSON(true, mainHall.name));
		this.chatRoomsOnServer.add(mainHall);
		this.lockedRooms = new ArrayList<String[]>();
		this.roomMsgs = new HashMap<String,BlockingQueue<RoomMessage>>();
	}

	public static synchronized ChatRoomsHandler getInstance(){
		if(instance == null){
			instance = new ChatRoomsHandler();
		}
		return instance;
	}

	public ChatRoom getMainHall(){
		return mainHall;
	}

	public String getServerName(String roomName){
		for(String[] array : otherChatRooms){
			if(array[1].equals(roomName)){
				return array[0];
			}
		}
		return null;
	}

	public LinkedList<ChatRoom> getChatRoomsOnServer(){
		return chatRoomsOnServer;
	}

	public synchronized void lockRoom(String[] room) {
		lockedRooms.add(room);
	}

	public synchronized void unlockRoom(String[] room){
		String[] toRemove = null;
		for(String[] a : lockedRooms){
			if(a[0].equals(room[0])&&a[1].equals(room[1]))
				toRemove = a;
		}
		lockedRooms.remove(toRemove);
		return;
	}

	public synchronized boolean lockedRoom(String name){
		for(String[] a : lockedRooms){
			if(a[1].equals(name))
				return true;
		}
		return false;
	}

	public boolean existingRoomOnServer(String name){
		for(ChatRoom cRoom : chatRoomsOnServer){
			if(cRoom.getName().equals(name)){
				return true;
			}
		}
		return false;
	}

	public boolean existingRoomOtherServers(String name){
		for(String[] array : otherChatRooms){
			if(array[1].equals(name)){
				return true;
			}
		}
		return false;
	}

	public boolean existingRoom(String name){
		return existingRoomOnServer(name)||existingRoomOtherServers(name);
	}

	@Override
	boolean existingOnServer(String name) {
		if(existingRoom(name)){
			return true;
		}
		if(lockedRoom(name)){
			return true;
		}
		return false;
	}

	public ChatRoom getChatRoomFromName(String name) {
		for(ChatRoom room : chatRoomsOnServer){
			if(room.name.equals(name)){
				return room;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public JSONObject list(JSONObject request){
		JSONObject answer = new JSONObject();
		answer.put("type", "roomlist");
		JSONArray rooms = new JSONArray();
		for(ChatRoom cRoom : chatRoomsOnServer){
			rooms.add(cRoom.name);
		}
		for(String[] array : otherChatRooms){
			rooms.add(array[1]);
		}
		answer.put("rooms", rooms);
		return answer;
	}

	public void newChatRoom(ClientHandler owner, JSONObject room){
		String name =  ((String) room.get("roomid"));
		if(!getChatRoomFromName(owner.getCurrentChatRoom()).isMainHall && getChatRoomFromName(owner.getCurrentChatRoom()).owner.equals(owner)){
			//the client cannot create a room, he owns the room he is in
			owner.write(refuseRoomCreation(name).toJSONString());
		}
		roomMsgs.put(name, new LinkedBlockingQueue<RoomMessage>());
		if(checkName(name)){
			roomMsgs.remove(name); 
			owner.write(grantRoomCreation(name).toJSONString());
			ClientChatRoom newRoom = new ClientChatRoom(owner, name);
			chatRoomsOnServer.add(newRoom);			
		}else{
			roomMsgs.remove(name); 
			owner.write(refuseRoomCreation(name).toJSONString());
		}
	}

	@SuppressWarnings("unchecked")
	private JSONObject grantRoomCreation(String name) {
		JSONObject grant = new JSONObject();
		grant.put("type", "createroom");
		grant.put("roomid", name);
		grant.put("approved", "true");
		return grant;
	}

	@SuppressWarnings("unchecked")
	private JSONObject refuseRoomCreation(String name) {
		JSONObject refusal = new JSONObject();
		refusal.put("type", "createroom");
		refusal.put("roomid", name);
		refusal.put("approved", "false");
		return refusal;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected JSONObject generateJSONLock(String name) {
		JSONObject lock = new JSONObject();
		lock.put("type", "lockroomid");
		lock.put("serverid", ServerIdentity.name);
		lock.put("roomid", name);
		return lock;
	}

	@Override
	protected boolean waitForAnswers(String name) throws InterruptedException {
		int n = ServerIdentity.otherServers.size();
		RoomMessage msg = null;
		while(n!=0){
			msg = roomMsgs.get(name).take();
			if(msg.locked == false){
				return false;
			}
			n--;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected JSONObject releaselockJSON(boolean approved, String name) {
		JSONObject release = new JSONObject();
		release.put("type", "releaseroomid");
		release.put("serverid", ServerIdentity.name);
		release.put("roomid", name);
		release.put("approved", Boolean.toString(approved));
		return release;
	}

	public void addNewRoomMessage(String name, RoomMessage roomMessage) {
		try {
			roomMsgs.get(name).put(roomMessage);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void acknowledgeRoomCreation(String[] room) {
		otherChatRooms.add(room);
	}

	@SuppressWarnings("unchecked")
	@Override
	protected JSONObject generateJSONVote(String name) {
		JSONObject vote = new JSONObject();
		vote.put("type", "lockroomid");
		vote.put("serverid", ServerIdentity.name);
		vote.put("roomid", name);
		if(existingOnServer(name)){
			vote.put("locked", "false");
		}else{
			vote.put("locked", "true");
		}
		return vote;
	}

	public JSONObject join(ClientHandler client, String name){
		if(getChatRoomFromName(client.getCurrentChatRoom()) != null){
			if(!getChatRoomFromName(client.getCurrentChatRoom()).isMainHall && getChatRoomFromName(client.getCurrentChatRoom()).owner.equals(client)){
				//the client cannot create a room, he owns the room he is in
				return refuseJoin(client);
			}
		}
		if(existingRoomOnServer(name))
			return getChatRoomFromName(name).join(client);
		if(existingRoomOtherServers(name)){
			getChatRoomFromName(client.getCurrentChatRoom()).quit(client);
			getChatRoomFromName(client.getCurrentChatRoom()).broadcastRoom(roomChange(client,name));
			IdentitiesHandler.getInstance().removeConnectedClient(client);
			//route the client to the chat room host
			return routeTo(ServerIdentity.otherServersForClients.get(getServerName(name)),name);
		}
		//refuse to join
		return refuseJoin(client);
	}

	@SuppressWarnings("unchecked")
	private JSONObject roomChange(ClientHandler client, String name){
		JSONObject roomchange = new JSONObject();
		roomchange.put("type", "roomchange");
		roomchange.put("identity", client.getMyName());
		roomchange.put("former", client.getCurrentChatRoom());
		roomchange.put("roomid", name);
		return roomchange;
	}

	@SuppressWarnings("unchecked")
	private JSONObject routeTo(DefSocket socket,String name) {
		JSONObject route = new JSONObject();
		route.put("type", "route");
		route.put("roomid", name);
		route.put("host", socket.address.getHostAddress());
		route.put("port", "" + socket.port);
		return route;
	}

	@SuppressWarnings("unchecked")
	private JSONObject refuseJoin(ClientHandler client){
		JSONObject roomchange = new JSONObject();
		roomchange.put("type", "roomchange");
		roomchange.put("identity", client.getMyName());
		roomchange.put("former", client.getCurrentChatRoom());
		roomchange.put("roomid", client.getCurrentChatRoom());
		return roomchange;
	}

	@SuppressWarnings("unchecked")
	public void message(ClientHandler sender, String msg){
		JSONObject mess = new JSONObject();
		mess.put("type", "message");
		mess.put("content",msg);
		mess.put("identity", sender.getMyName());
		getChatRoomFromName(sender.getCurrentChatRoom()).broadcastMessage(mess);
	}

	public JSONObject deleteChatRoom(String name, ClientHandler client){
		ChatRoom room = getChatRoomFromName(name);
		if(room == null){
			//room that does not exists
			return refuseDeletion(name);
		}
		if(room.isMainHall||(!room.name.equals(client.getCurrentChatRoom()))){
			//deleting mainhall or room different that the current room
			return refuseDeletion(name);
		}
		if(room.owner.equals(client)){
			((ClientChatRoom)room).delete(client);
			chatRoomsOnServer.remove(room);
			return grantDeletion(name);
		}else{
			return refuseDeletion(name);
		}
	}

	@SuppressWarnings("unchecked")
	JSONObject refuseDeletion(String name){
		JSONObject refuse = new JSONObject();
		refuse.put("type", "deleteroom");
		refuse.put("roomid", name);
		refuse.put("approved", "false");
		return refuse;
	}

	@SuppressWarnings("unchecked")
	JSONObject grantDeletion(String name){
		JSONObject refuse = new JSONObject();
		refuse.put("type", "deleteroom");
		refuse.put("roomid", name);
		refuse.put("approved", "true");
		return refuse;
	}

	public void acknowledgeRoomDeletion(JSONObject obj) {
		String[] array = {(String)obj.get("serverid"),(String)obj.get("roomid")};
		String[] toRemove = null;
		for(String[] a : otherChatRooms){
			if(a[0].equals(array[0])&&a[1].equals(array[1])){
				toRemove = a;
				break;
			}
		}
		otherChatRooms.remove(toRemove);
		return;
	}

	public JSONObject quit(ClientHandler clientHandler) {
		ChatRoom chatRoom = ChatRoomsHandler.getInstance().getChatRoomFromName(clientHandler.getCurrentChatRoom());
		IdentitiesHandler.getInstance().removeConnectedClient(clientHandler);
		if(chatRoom.owner == clientHandler){
			ClientChatRoom room = (ClientChatRoom)chatRoom;
			room.broadcastMessage(quitJSON(clientHandler,chatRoom));
			room.deleteAndQuit(clientHandler);
			((ClientChatRoom)room).delete(clientHandler);
			chatRoomsOnServer.remove(room);
			return quitJSON(clientHandler,chatRoom);
		}else{
			chatRoom.quit(clientHandler);
			chatRoom.broadcastMessage(quitJSON(clientHandler,chatRoom));
			return quitJSON(clientHandler,chatRoom);	
		}
	}

	@SuppressWarnings("unchecked")
	public JSONObject quitJSON(ClientHandler client, ChatRoom chatRoom){
		JSONObject roomchange = new JSONObject();
		roomchange.put("type", "roomchange");
		roomchange.put("identity", client.getMyName());
		roomchange.put("former", chatRoom.name);
		roomchange.put("roomid", "");
		return roomchange;
	}
}