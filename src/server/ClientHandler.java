package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ClientHandler extends Thread {

	private BufferedReader readerClient;
	private BufferedWriter writerClient;

	private String MyName;
	private String currentChatRoom;

	private boolean endConnection = false;
	
	public ClientHandler(ServerIdentity serverId, Socket clientSocket) throws UnsupportedEncodingException, IOException {

		this.readerClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream(), "UTF-8"));
		this.writerClient = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream(), "UTF-8"));

		this.MyName = null;
		this.currentChatRoom = null;
	}

	public String getMyName() {
		return MyName;
	}

	public void setMyName(String name){
		this.MyName = name;
	}
	
	public String getCurrentChatRoom() {
		return currentChatRoom;
	}

	public void setCurrentChatRoom(ChatRoom currentChatRoom) {
		this.currentChatRoom = currentChatRoom.name;
	}

	private void process(JSONObject request) {
		JSONObject answer = null;
		switch((String)request.get("type")){ 
		case "newidentity":
			answer = IdentitiesHandler.getInstance().newIdentity(this,request);
			write(answer.toJSONString());
			if(((String)answer.get("approved")).equals("true")){
				write(ChatRoomsHandler.getInstance().getMainHall().join(this).toJSONString());
			}else{
				//the identity was refuse
				endConnection = true;
			}
			break;
		case "list":
			write(ChatRoomsHandler.getInstance().list(request).toJSONString());
			break;
		case "who":
			write(ChatRoomsHandler.getInstance().getChatRoomFromName(currentChatRoom).list(request).toJSONString());
			break;
		case "createroom":
			ChatRoomsHandler.getInstance().newChatRoom(this, request);
			break;
		case "join":
			answer = ChatRoomsHandler.getInstance().join(this, (String)request.get("roomid"));
			write(answer.toJSONString());
			if(answer.get("type").equals("route")){
				//the room is on an other server
				endConnection = true;
			}
			break;
		case "movejoin":
			this.MyName = (String)request.get("identity");
			this.currentChatRoom = (String)request.get("former");
			if(ChatRoomsHandler.getInstance().existingOnServer((String)request.get("roomid"))){
				//the wanted room still exists
				write(ChatRoomsHandler.getInstance().join(this, (String)request.get("roomid")).toJSONString());
			}else{
				//the wanted room does not exists anymore
				write(ChatRoomsHandler.getInstance().getMainHall().join(this).toJSONString());
			}
			break;
		case "deleteroom":
			write(ChatRoomsHandler.getInstance().deleteChatRoom((String)request.get("roomid"),this).toJSONString());
			break;
		case "message":
			ChatRoomsHandler.getInstance().message(this, (String)request.get("content"));
			break;
		case "quit":
			endConnection = true;
			write(ChatRoomsHandler.getInstance().quit(this).toJSONString());
			break;
		default:
			throw new IllegalArgumentException("Unsupported request");
		}
	}

	@Override
	public void run() {
		try {
			JSONObject request = null;
			while(true){

				JSONParser parser = new JSONParser();
				String requestJSON = readerClient.readLine();
				if(requestJSON == null){
					//means that the connection has been lost
					ChatRoomsHandler.getInstance().quit(this);
					break;
				}else{
					request = (JSONObject) parser.parse(requestJSON);
				}
				process(request);
				if(endConnection){
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void write(String msg) {
		try {
			writerClient.write(msg + "\n");
			writerClient.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


}
