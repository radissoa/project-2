package server;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.util.ArrayList;

import org.json.simple.JSONObject;

public class ClientChatRoom extends ChatRoom {

	public ClientChatRoom(ClientHandler clientHandler, String name) {
		super();
		super.name = name;
		super.owner = clientHandler;
		clientHandler.write(join(clientHandler).toJSONString());
	}

	synchronized void delete(ClientHandler client) {
		int n = chatters.size();
		while(n != 0){
			ClientHandler c = chatters.poll();
			JSONObject roomchange = ChatRoomsHandler.getInstance().getMainHall().join(c);
			c.write(roomchange.toJSONString());
			n--;
		}
		JSONObject object = deleteRoomJSON();
		ArrayList<Socket> sockets = new ArrayList<Socket>();
		//Launching sockets to the other servers
		for(DefSocket dS : ServerIdentity.otherServers.values()){
			try {
				sockets.add(new Socket(dS.address, dS.port));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//signal the release
		BufferedWriter writer = null;
		for(Socket s : sockets){
			try {
				writer = new BufferedWriter(new OutputStreamWriter(s.getOutputStream(), "UTF-8"));
				writer.write(object.toJSONString());
				writer.flush();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for(Socket s : sockets){
			if(s !=null){
				try{
					s.close();
				}catch(IOException e){
					e.printStackTrace();
				}
			}
		}
	}

	synchronized void deleteAndQuit(ClientHandler client){
		int n = chatters.size();
		while(n != 0){
			ClientHandler c = chatters.poll();
			if(c.equals(client)){
				n--;
				continue;
			}
			c.write(ChatRoomsHandler.getInstance().getMainHall().join(c).toJSONString());
			n--;
		}
		JSONObject object = deleteRoomJSON();
		ArrayList<Socket> sockets = new ArrayList<Socket>();
		//Launching sockets to the other servers
		for(DefSocket dS : ServerIdentity.otherServers.values()){
			try {
				sockets.add(new Socket(dS.address, dS.port));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		//signal the release
		BufferedWriter writer = null;
		for(Socket s : sockets){
			try {
				writer = new BufferedWriter(new OutputStreamWriter(s.getOutputStream(), "UTF-8"));
				writer.write(object.toJSONString());
				writer.flush();
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		for(Socket s : sockets){
			if(s !=null){
				try{
					s.close();
				}catch(IOException e){
					e.printStackTrace();
				}
			}
		}


	}
	
	@SuppressWarnings("unchecked")
	private JSONObject deleteRoomJSON() {
		JSONObject object = new JSONObject();
		object.put("type", "deleteroom");
		object.put("serverid", ServerIdentity.name);
		object.put("roomid", name);
		return object;
	}
}
