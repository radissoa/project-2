package server;

import java.util.ArrayList;

import org.json.simple.JSONArray;

public class ServMsg {
	
	String name;
	ArrayList<String> cRooms;
	
	public ServMsg(String name, JSONArray a){
		this.name = name;
		this.cRooms = new ArrayList<>();
		for(int i = 0; i < a.size(); i++){
			cRooms.add((String)a.get(i));
		}
	}
}
