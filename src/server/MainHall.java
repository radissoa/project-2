package server;

public class MainHall extends ChatRoom {

	public MainHall(String serverId) {
		super();
		super.isMainHall = true;
		super.name ="MainHall-" + serverId;
	}
}
