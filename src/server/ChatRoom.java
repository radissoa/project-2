package server;

import java.util.LinkedList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

abstract public class ChatRoom {

	public boolean isMainHall = false;
	
	protected String name;

	protected ClientHandler owner;

	protected LinkedList<ClientHandler> chatters;

	public ChatRoom() {
		this.owner = null;
		this.chatters = new LinkedList<ClientHandler>();
	}

	public String getName(){
		return name;
	}

	@SuppressWarnings("unchecked")
	public JSONObject list(JSONObject request) {
		JSONObject answer = new JSONObject();
		answer.put("type", "roomcontents");
		answer.put("roomid", name);
		JSONArray identities = new JSONArray();
		for(ClientHandler client : chatters){
			identities.add(client.getMyName());
		}
		answer.put("identities", identities);
		if(owner != null){
			answer.put("owner", owner.getMyName());
		}else{
			answer.put("owner", "");
		}
		return answer;
	}

	public synchronized void addClient(ClientHandler client){
		this.chatters.add(client);
	}

	public synchronized void quit(ClientHandler chatClient){
		this.chatters.remove(chatClient);
	}

	public void broadcastMessage(JSONObject obj){
		for(ClientHandler handler : chatters){
			if(handler.getMyName().equals((String)obj.get("identity")))
				continue;
			handler.write(obj.toJSONString());
		}
	}

	public void broadcastRoom(JSONObject obj){
		for(ClientHandler handler : chatters){
			handler.write(obj.toJSONString());
		}
	}

	@SuppressWarnings("unchecked")
	public synchronized JSONObject join(ClientHandler client) {
		JSONObject roomchange = new JSONObject();
		roomchange.put("type", "roomchange");
		roomchange.put("identity", client.getMyName());
		if(client.getCurrentChatRoom() != null){
			roomchange.put("former", client.getCurrentChatRoom());
		}else{
			//when the client launches he doesn't have a former chat room
			roomchange.put("former", "");
		}
		roomchange.put("roomid", this.name);
		if(client.getCurrentChatRoom() != null){
			if(ChatRoomsHandler.getInstance().existingRoomOnServer(client.getCurrentChatRoom())){
				/*the client has a former chat room on the server: 
				 *signaling the chatters of the former chatters my leave
				 */
				ChatRoomsHandler.getInstance().getChatRoomFromName(client.getCurrentChatRoom()).quit(client);
				ChatRoomsHandler.getInstance().getChatRoomFromName(client.getCurrentChatRoom()).broadcastRoom(roomchange);
			}else{
				IdentitiesHandler.getInstance().addConnectedClient(client);
				JSONObject serverchange = new JSONObject();
				serverchange.put("type", "serverchange");
				serverchange.put("approved", "true");
				serverchange.put("serverid", ServerIdentity.name);
				client.write(serverchange.toJSONString());
			}
		}
		broadcastRoom(roomchange);
		addClient(client);
		client.setCurrentChatRoom(this);
		return roomchange;
	}
}
