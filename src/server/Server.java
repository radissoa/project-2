package server;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.kohsuke.args4j.*;

/**
 * Main
 * @author radissoa
 *
 */

public class Server {

	static BlockingQueue<ServMsg> servMsgs =new ArrayBlockingQueue<ServMsg>(15);

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {

		CmdLineArgs argsBean = new CmdLineArgs();
		CmdLineParser parser = new CmdLineParser(argsBean);
		ServerSocket listeningSocket = null;

		try {
			parser.parseArgument(args);

			ServerIdentity serverId = new ServerIdentity(argsBean.getConfPath());		

			Socket s = new Socket(argsBean.getHost(), Integer.parseInt(argsBean.getPort()));

			BufferedReader reader = new BufferedReader(new InputStreamReader(s.getInputStream(), "UTF-8"));
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(s.getOutputStream(), "UTF-8"));

			JSONObject askAdmin = new JSONObject();
			askAdmin.put("type", "newserver");
			askAdmin.put("serverid", ServerIdentity.name);
			askAdmin.put("ip", ServerIdentity.myIp.getHostName());
			askAdmin.put("port", "" + ServerIdentity.clientPort);
			askAdmin.put("coordport", "" + ServerIdentity.coordinationPort);
			writer.write(askAdmin.toJSONString() + "\n");
			writer.flush();

			JSONObject answerAdmin = (JSONObject) new JSONParser().parse(reader.readLine());
			if(answerAdmin.get("type").equals("newserver")){
				if(answerAdmin.get("approved").equals("false")){
					System.out.println("Name used..");
					s.close();
					return;
				}else{
					s.close();
				}
			}else{
				s.close();
				throw new IllegalArgumentException();
			}
			
			ChatRoomsHandler.getInstance();

			//acquiring the servers
			JSONArray servers = (JSONArray) answerAdmin.get("servers");
			JSONObject o = null;
			for(int i = 0; i < servers.size(); i++){
				o = (JSONObject) servers.get(i);
				ServerIdentity.addServer("" + o.get("serverid"), 
						InetAddress.getByName("" + o.get("ip")), 
						Integer.parseInt("" + o.get("port")), 
						Integer.parseInt("" + o.get("coordport")));
			}
			
			Coordination coord = new Coordination();
			coord.setName("Thread Coord");
			coord.start();
			
			//tell the other servers that I exist
			JSONObject me = me();
			Locktools.broadcastServer(me);
			
			//wait for other servers answers
			int n = ServerIdentity.otherServers.size();
			ServMsg msg;
			while(n!=0){
				msg = servMsgs.take();
				for(String c :msg.cRooms){
					String[] room = {msg.name, c};
					ChatRoomsHandler.getInstance().acknowledgeRoomCreation(room);
				}
				n--;
			}
			
			//Tell them that I have a main hall
			Locktools.broadcastServer(ChatRoomsHandler.getInstance().releaselockJSON(true, ChatRoomsHandler.getInstance().getMainHall().name));
			
			listeningSocket = new ServerSocket(ServerIdentity.clientPort);
			
			n = 0;
			while(true){

				Socket clientSocket = listeningSocket.accept();
				ClientHandler clientHandler = new ClientHandler(serverId, clientSocket);
				clientHandler.setName("Thread Client-" + n);
				n++;
				clientHandler.start();
			}
		} catch (CmdLineException e) {
			parser.printUsage(System.out);
		} catch (FileNotFoundException fE) {
			System.out.println(fE.getMessage());
		} catch (IOException e){
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}finally{
			if(listeningSocket != null) {
				try {
					listeningSocket.close();
				} catch (IOException e) {
					System.err.println("Error while closing server listening socket.");
				}
			}

		}
	}

	@SuppressWarnings("unchecked")
	private static JSONObject me() {
		JSONObject o = new JSONObject();
		o.put("type", "newserver");
		o.put("serverid", ServerIdentity.name);
		o.put("ip", ServerIdentity.myIp.getHostName());
		o.put("port", "" + ServerIdentity.clientPort);
		o.put("coordport", "" + ServerIdentity.coordinationPort);
		return o;
	}
}
