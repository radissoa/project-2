package server;

import org.json.simple.JSONObject;

public class ConnectionMessage {

	String serverId;
	boolean locked;
	
	public ConnectionMessage(JSONObject obj){
		this.serverId = (String) obj.get("serverid");
		this.locked = ((String)obj.get("locked")).equals("true");
	}
}
