package server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import org.json.simple.JSONObject;

public class IdentitiesHandler extends Locktools {

	private static IdentitiesHandler instance;

	private ArrayList<ClientHandler> connectedClients;
	private ArrayList<String[]> lockedIdentity;
	private HashMap<String,BlockingQueue<ConnectionMessage>> connectionMsgs;

	private IdentitiesHandler() {
		this.connectedClients = new ArrayList<ClientHandler>();
		this.lockedIdentity = new ArrayList<String[]>();
		this.connectionMsgs = new HashMap<String,BlockingQueue<ConnectionMessage>>();
	}

	public static synchronized IdentitiesHandler getInstance(){
		if(instance == null){
			instance = new IdentitiesHandler();
		}
		return instance;
	}
	
	public boolean existingClient(String name){
		for(ClientHandler cHandler : connectedClients){
			if(cHandler.getMyName().equals(name)){
				return true;
			}
		}
		return false;
	}

	public synchronized void addConnectedClient(ClientHandler client) {
		connectedClients.add(client);
	}

	public synchronized void removeConnectedClient(ClientHandler client){
		connectedClients.remove(client);
	}
	
	@Override
	boolean existingOnServer(String name) {
		if(existingClient(name) || lockedIdentity(name)){
			return true;
		}	
		return false;
	}

	public synchronized void lockIdentity(String[] identity) {
		lockedIdentity.add(identity);
	}

	public synchronized void unlockIdentity(String[] identity){
		String[] toRemove = null;
		for(String[] array : lockedIdentity){
			if(array[0].equals(identity[0]) &&array[1].equals(identity[1])){
				toRemove = array;
				break;
			}
		}
		if(toRemove != null){
			lockedIdentity.remove(toRemove);
		}
	}

	public boolean lockedIdentity(String name){
		for(String[] a : lockedIdentity){
			if(a[1].equals(name))
				return true;
		}
		return false;
	}

	public synchronized void addConnectionMessage(String name, ConnectionMessage msg){
		try {
			connectionMsgs.get(name).put(msg);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public JSONObject newIdentity(ClientHandler cHandler, JSONObject request){
		String name = ((String) request.get("identity"));
		//register the connection of client 
		connectionMsgs.put(name, new LinkedBlockingQueue<ConnectionMessage>());
		if(checkName(name)){
			cHandler.setMyName(name);
			connectionMsgs.remove(cHandler.getMyName());
			addConnectedClient(cHandler);
			return grantIdentityCreation();
		}else{
			connectionMsgs.remove(name);
			return refuseIdentityCreation();
		}
	}

	@SuppressWarnings("unchecked")
	private JSONObject grantIdentityCreation() {
		JSONObject refusal = new JSONObject();
		refusal.put("type", "newidentity");
		refusal.put("approved", "true");
		return refusal;
	}

	@SuppressWarnings("unchecked")
	private JSONObject refuseIdentityCreation() {
		JSONObject refusal = new JSONObject();
		refusal.put("type", "newidentity");
		refusal.put("approved", "false");
		return refusal;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected JSONObject releaselockJSON(boolean approved, String name) {
		JSONObject release = new JSONObject();
		release.put("type", "releaseidentity");
		release.put("serverid", ServerIdentity.name);
		release.put("identity", name);
		return release;
	}

		@SuppressWarnings("unchecked")
	@Override
	protected JSONObject generateJSONLock(String name) {
		JSONObject lock = new JSONObject();
		lock.put("type", "lockidentity");
		lock.put("serverid", ServerIdentity.name);
		lock.put("identity", name);
		return lock;
	}

	@Override
	protected boolean waitForAnswers(String name) throws InterruptedException {
		int n = ServerIdentity.otherServers.size();
		ConnectionMessage msg = null;
		while(n!=0){
			msg = connectionMsgs.get(name).take();
			if(msg.locked == false){
				return false;
			}
			n--;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected JSONObject generateJSONVote(String name) {
		JSONObject vote = new JSONObject();
		vote.put("type", "lockidentity");
		vote.put("serverid", ServerIdentity.name);
		vote.put("identity", name);
		if(existingOnServer(name)){
			vote.put("locked", "false");
		}else{
			vote.put("locked", "true");
		}
		return vote;
	}


}
