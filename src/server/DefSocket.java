package server;

import java.net.InetAddress;

public class DefSocket {
	InetAddress address;
	int port;

	public DefSocket(InetAddress addr, int port){
		this.address = addr;
		this.port = port;
	}
}
