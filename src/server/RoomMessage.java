package server;

import org.json.simple.JSONObject;

public class RoomMessage {
	String serverId;
	boolean locked;
	
	public RoomMessage(JSONObject obj){
		this.serverId = (String) obj.get("serverid");
		this.locked = ((String)obj.get("locked")).equals("true");
	}
}
